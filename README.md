# Animació amb HTML, JavaScript i CSS
Aquesta animació la hem realitzada Nil Gomis i Luis Clavero.

# Posada en marxa
Degut a que utilitzem posicions no dinàmiques i píxels abans de fer res utilitzar el 100% d'amplada i alçada amb la finestra per a que es vegi bé.

Durant l'animació s'escolta un soroll de Mario "menjant-se" el bolet per crèixer, aquest so es fa automàticament i alguns navegadors per defecte no permeten l'autoplay de les pàgines web, per tant pot no escoltar-se, deixem aquí algunes guies per a desactivar aquesta funció i per tant que es pugui escoltar el so:
* [Guia per a Google Chrome](https://www.ghacks.net/2018/02/06/how-to-control-audio-and-video-autoplay-in-google-chrome/)
* [Guia per a Firefox](https://support.mozilla.org/en-US/kb/block-autoplay)
* [Guia per a Safari](https://www.pilatesanytime.com/Pilates-Help/1016/How-to-Get-Safari-to-Autoplay-Video-and-Audio-Chapters)
* [Guia per a Microsoft Edge](https://www.tenforums.com/tutorials/159100-how-enable-disable-media-autoplay-microsoft-edge-chromium.html)

L'únic que s'ha de fer a part d'això és obrir el fitxer "smb.html".
