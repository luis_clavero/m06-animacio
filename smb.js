// Get the super mario bros image
var mario = document.querySelector("#mario");

// Get the mushroom image
var mushroom = document.querySelector("#mushroom");

// Get the audio of the mushroom
const mushroomAudio = new Audio('marioMushroom.mp3');

// Mario animation configuration
var marioStart = {"left" : "250px"};
var marioEnd = {"left" : "800px"};
var movementDuration = {"duration" : 4000};
var marioSize1 = {"transform" : "scale(1)"};
var marioSize2 = {"transform" : "scale(2)"};
var transformDuration = {"duration" : 1500};

// Mushroom animation configuration
var mushroomMoveStart = {"left" : "1500px"};
var mushroomMoveEnd = {"left" : "885px"};
var mushroomMoveDuration = {"duration" : 2750}
var mushroomJumpStart = 890;
var mushroomJumpEnd = 790;
var jumpRefreshRate = 1000 / 60; // Divide a second in 60 part to achieve 60 frames per second so it looks smooth
var speedX = -3.5; /* Speed of the animation, it has to be a negative value since we want the mushroom to 
go upwards and downwards and the less the top value is the closer to the top of the page */
var positionX = 890;

// Make the mushroom go from near the tube to near the middle of the page
mushroom.animate([mushroomMoveStart, mushroomMoveEnd], mushroomMoveDuration).onfinish = 
// Move the mushroom up and down repeatedly
window.setInterval(() => {
  positionX = positionX + speedX;
  if (positionX > mushroomJumpStart || positionX < mushroomJumpEnd) {
    speedX = speedX * (-1);
  }
  mushroom.style.top = positionX + 'px';
}, jumpRefreshRate);

// Move the mario to the mushroom and make it stay there
mario.animate([marioStart, marioEnd], movementDuration).onfinish = mario.style.left = "800px";

// Make the image blink
setTimeout(function(){ mario.style.animation = "blink 0.5s linear infinite"; }, 4500);

// Animate the transformation of the size of the mario
setTimeout(function(){
    // Change the top value to look properly
    mario.style.top = "810px";
    // Hide the mushroom
    mushroom.style.visibility = "hidden";
    // Play the sound of the mario eating the mushroom
    mushroomAudio.play();
    mario.animate([marioSize1, marioSize2], transformDuration).onfinish = function() {
                      // Change the size of the mario and change it's top 
                      mario.style.width = "200px";
                      mario.style.height = "200px";
                      mario.style.top = "760px";
                      mario.style.left = "750px";
                      mario.style.animation = "";
                      // Make the mario go near the tube
                      marioStart = {"left" : "750px"};
                      marioEnd = {"left" : "1100px"};
                      movementDuration = {"duration" : 2000}
                      mario.animate([marioStart, marioEnd], movementDuration);
                      mario.style.left = "1100px";
}}, 4200);

setTimeout(function(){
  var fireball = document.querySelector("#fb");
  fireball.style.display = "block";
  
},5100);

setTimeout(function(){
  var fireball = document.querySelector("#fb");
  fireball.style.display = "none";
  // Turn on the blink animation
  mario.style.animation = "blink 0.5s linear infinite";
  // Decrease the size of the mario
  marioSize1 = {"transform" : "scale(1)"};
  marioSize2 = {"transform" : "scale(0.5)"};
  mario.animate([marioSize1, marioSize2], transformDuration).onfinish = () => {
    mario.style.width = "100px";
    mario.style.height = "100px";
    mario.style.top = "860px";
    mario.style.left = "1150px";
    mario.style.animation = "";
  }
},7700);
